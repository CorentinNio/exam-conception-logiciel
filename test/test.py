import unittest 
from client.main import nombre_par_couleurs

class testNombreCouleurs(unittest.TestCase) :

    def test_nombre_couleurs(self) :
        cartes = [{"value": "KING","suit": "SPADES","code": "KS"},
                {"value": "2","suit": "HEARTS","code": "2H"},
                {"value": "QUEEN","suit": "CLUBS","code": "QC"},
                {"value": "10","suit": "SPADES","code": "10S"},
                {"value": "KING","suit": "HEARTS","code": "KH"},
                {"value": "7","suit": "CLUBS","code": "7C"},
                {"value": "9","suit": "DIAMONDS","code": "9D"},
                {"value": "8","suit": "CLUBS","code": "8C"},
                {"value": "3","suit": "CLUBS","code": "3C"},
                {"value": "9","suit": "HEARTS","code": "9H"},
            ]
        nombre_par_couleurs_test = nombre_par_couleurs(cartes)
        nombre_par_couleurs_réel = {"H":3,"S":2,"D":1,"C":4}
        self.assertEqual(nombre_par_couleurs_test,nombre_par_couleurs_réel)

if __name__ == "__main__":
    unittest.main()




