import requests

def init_deck():
    response = requests.get("http://localhost:8000/creer-un-deck/")
    return(response.json())


def tirer_cartes(nombre_cartes : int, deck_id : dict) :
    response = requests.post("http://localhost:8000/cartes/{}".format(nombre_cartes) , json=deck_id)
    return(response.json()["cartes"])

def nombre_par_couleurs(cartes : list) :
    couleurs = {"H":0,"S":0,"D":0,"C":0}
    for i in range(len(cartes)) :
        if cartes[i]["suit"] == "HEARTS" :
            couleurs["H"] += 1
        if cartes[i]["suit"] == "SPADES" :
            couleurs["S"] += 1
        if cartes[i]["suit"] == "DIAMONDS" :
            couleurs["D"] += 1
        if cartes[i]["suit"] == "CLUBS" :
            couleurs["C"] += 1
    return(couleurs)

if __name__ == "__main__":
    deck_id = init_deck()
    cartes = tirer_cartes(10, deck_id)
    couleurs = nombre_par_couleurs(cartes)
    print(deck_id,couleurs)

