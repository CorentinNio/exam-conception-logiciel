from fastapi import FastAPI
import requests

app = FastAPI()

@app.get("/")
def read_root():
    return {"Hello" : "world"}

@app.get("/creer-un-deck/")
def recup_deck():
    deck = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
    return {"deck_id": deck.json()["deck_id"]}

@app.post("/cartes/{nombre_cartes}")
async def tirer_cartes(nombre_cartes : int, deck_id : dict):
    result = requests.get("https://deckofcardsapi.com/api/deck/"+deck_id["deck_id"]+"/draw/?count={}".format(nombre_cartes))
    return {"deck_id" : deck_id["deck_id"], "cartes" : result.json()["cards"]}
