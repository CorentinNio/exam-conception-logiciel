# Examen Conception logiciel

L'application dévelopée dans le cadre de cet examen se décompose en 3 parties :
* une partie webservice, qui permet de créer une API local. Elle fait elle même appelle à l'API https://deckofcardsapi.com/
* une partie client, qui va faire appelle à l'API créer par le webservice afin d'initialiser un deck, de tirer des cartes de ce deck ou bien encore de déterminer le nombre de cartes de chaque couleur pour un ensemble de cartes contenus dans une liste.
* une partie test permettant de faire un test unitaire pour tester la fonctionnalité de la dernière fonction décrite ci-dessus

Commencer par cloner le dépot git de ce TP.
Puis placer vous à la racine du projet.
Lancer ensuite la ligne de commande suivante pour installer les packages nécessaires au lancement de l'application :
```python
pip install -r requirements.txt
```

## 1. Le webservice

Pour lancer le webservice, lancer les deux lignes de commande suivantes : 
```python
cd webservice
uvicorn main:app --reload
```
L'adresse de l'API ainsi créé est http://localhost:8000


## 2. Le client

Une fois le webservice lancé, vous pouvez lancer la partie client. Pour ce faire, ouvrez un nouveau terminal, placez vous dans la partie client :
```python
cd client
```

Puis en fonction de la version de python dont vous disposez, lancez une des deux lignes de commande suivante :
```python
python main.py
python3 main.py
```
Cela permet d'initialiser un deck, d'en tirer 10 cartes, et de calculer le nombre de cartes tirées de chaque couleur. Ce qui est renvoyer est l'identifiant du deck sous la forme {deck_id:str} et le résultat du calcul du nombre de cartes tirées de chaque couleur sous la forme {"H":int,"S":int,"D":int,"C":int}.


## 3. Le test

Enfin, le test de la fonction de calcul du nombre de cartes de chaque couleur peut-être lancé depuis la racine du projet grâce à une des deux lignes de code suivante :
```python
python -m unittest Tests/test.py
python3 -m unittest Tests/test.py
```


 
